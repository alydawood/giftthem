from . import views
from django.urls import path,include


urlpatterns = [
    path('',views.home,name='gift_home'),
    path('create', views.CreateGift.as_view(), name='create_gift'),
    path('<int:pk>/delete', views.DeleteGift.as_view(), name='delete_gift'),
    path('<int:pk>/update', views.UpdateGift.as_view(), name='update_gift'),
]
