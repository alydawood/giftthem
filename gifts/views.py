from django.shortcuts import render,redirect, get_object_or_404
from django.urls import reverse_lazy
from django.views import generic
from .models import Gifts
from  django.contrib.auth.forms import UserCreationForm
from  django.contrib.auth import authenticate,login
from django.forms.utils import ErrorList
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
import urllib
import requests
# Create your views here.
def home(req):
    if(req.user.is_anonymous):
        return redirect('login')
    gifts = Gifts.objects.all().filter(user=req.user);
    return render(req,'gifts/gift_home.html',{"gifts":gifts})


class CreateGift(generic.CreateView):
    model= Gifts
    fields= ['fname','lname','giftname','image']

    template_name="gifts/create_gift.html"
    success_url= reverse_lazy('gift_home')

    def form_valid(self, form):
        form.instance.user = self.request.user
        super(CreateGift, self).form_valid(form)
        return redirect('gift_home')

class DeleteGift(generic.DeleteView):
    model= Gifts
    success_url=reverse_lazy('gift_home')

class UpdateGift(generic.UpdateView):
    model = Gifts
    template_name = 'gifts/update_gift.html'
    fields= ['fname','lname','giftname','image']
    success_url=reverse_lazy('gift_home')

class SignUp(generic.CreateView):
    form_class=UserCreationForm
    success_url=reverse_lazy('gift_home')
    template_name='registration/signup.html'
