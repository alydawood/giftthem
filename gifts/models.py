from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Gifts(models.Model):
    fname=models.CharField("First name",max_length=25)
    lname=models.CharField("Last name",max_length=25)
    giftname=models.CharField("Gift name",max_length=50)
    image=models.ImageField(upload_to='images/')
    user = models.ForeignKey(User, on_delete=models.CASCADE)
